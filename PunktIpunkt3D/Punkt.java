package PunktIpunkt3D;

public class Punkt {
    int x;
    int y;

    int pobierzX () {
        return x;
    }
    int pobierzY () {
        return y;
    }
    void ustawX (int wspX) {
        x = wspX;
    }
    void ustawY (int wspY) {
        y = wspY;
    }
    void ustawXY (int wspX, int wspY) {
        x = wspX;
        y = wspY;
    }
    void ustawXY (Punkt punkt) {
        x = punkt.x;
        y = punkt.y;
    }
    void wyswietlWspolrzedne () {
        System.out.println("Współrzędna x = " + x);
        System.out.println("Współrzędna y = " + y);
    }
    Punkt() {
        x = 2;
        y = 2;
    }

    Punkt(int wspX, int wspY) {
        x = wspX;
        y = wspY;
    }

    Punkt(Punkt3D punkt) {
        x = punkt.x;
        y = punkt.y;
    }
}

class Punkt3D extends Punkt {
    int z;

    void ustawZ(int wspZ) {
        z = wspZ;
    }

    int pobierzZ() {
        return z;
    }

    void ustawXYZ(int wspX, int wspY, int wspZ) {
        x = wspX;
        y = wspY;
        z = wspZ;
    }

    Punkt3D() {
        super();
        z = 1;
    }

    Punkt3D(int wspX, int wspY, int wspZ) {
        super(wspX, wspY);
        z = wspZ;
    }

    Punkt3D(Punkt3D punkt) {
        super(punkt);
        z = punkt.z;
    }
}