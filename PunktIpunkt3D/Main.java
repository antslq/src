package PunktIpunkt3D;

public class Main {
    public static void main(String args []) {
        Punkt3D punkt = new Punkt3D();

        System.out.println("x = " + punkt.x);
        System.out.println("y = " + punkt.y);
        System.out.println("z = " + punkt.z);
        System.out.println("");

        punkt.ustawX(100);
        punkt.ustawY(200);

        System.out.println("x = " + punkt.x);
        System.out.println("y = " + punkt.y);
        System.out.println("z = " + punkt.z);
        System.out.println("");

        punkt.ustawXY(300, 400);

        System.out.println("x = " + punkt.x);
        System.out.println("y = " + punkt.y);
        System.out.println("z = " + punkt.z);
        System.out.println("");

        Punkt3D punkt1 = new Punkt3D(111, 222, 333);

        System.out.println("x = " + punkt1.x);
        System.out.println("y = " + punkt1.y);
        System.out.println("z = " + punkt1.z);
        System.out.println("");

        Punkt3D punkt2 = new Punkt3D(punkt);

        System.out.println("x = " + punkt2.x);
        System.out.println("y = " + punkt2.y);
        System.out.println("z = " + punkt2.z);
        System.out.println("");

        Punkt3D punkt3 = new Punkt3D();

        System.out.println("x = " + punkt3.x);
        System.out.println("y = " + punkt3.y);
        System.out.println("z = " + punkt3.z);
        System.out.println("");

        Punkt3D punkt4 = new Punkt3D(11, 22, 33);

        System.out.println("x = " + punkt4.x);
        System.out.println("y = " + punkt4.y);
        System.out.println("z = " + punkt4.z);
        System.out.println("");

        Punkt3D punkt5 = new Punkt3D(punkt1);

        System.out.println("x = " + punkt5.x);
        System.out.println("y = " + punkt5.y);
        System.out.println("z = " + punkt5.z);
        System.out.println("");

        KolorowyPunkt kolor1 = new KolorowyPunkt();

        System.out.println("Kolor: " + kolor1.kolor);

        KolorowyPunkt kolor2 = new KolorowyPunkt("czerwony");

        System.out.println("Kolor: " + kolor2.kolor);

        KolorowyPunkt kolor3 = new KolorowyPunkt(kolor1);

        System.out.println("Kolor: " + kolor3.kolor);
    }
}
