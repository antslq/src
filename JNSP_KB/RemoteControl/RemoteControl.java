package JNSP_KB.RemoteControl;

import java.util.List;

//https://www.youtube.com/watch?v=EhmBjOjW0z8
//pilot
public class RemoteControl {

        private static final int LENGHT_IN_CM = 15;
        private static final int WIDTH_IN_CM = 4;
        private static final int THICKNESS_IN_CM = 1;

        private static final List<String> BUTTINS = List.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "Power", "VolumeDown", "VolumeUp", "NextChannel", "PreviousChanel");
    private final String color;

    private int currentVolume;
        private int currentChannel;

        public RemoteControl (String color) {
            this.color = color;
        }

        public void volumeUp () {
            currentVolume++;
        }

        public void volumeDown () {
            currentVolume--;
        }

        public void nextChannel () {
            currentChannel++;
        }

        public void previousChannel () {
            currentChannel--;
        }

        public int getCurrentVolume () {
            return currentVolume;
        }

        public int getCurrentChannel () {
            return currentChannel;
        }

        public void setCurrentVolume (int volume) {
            currentVolume = volume;
        }

        public void setCurrentChannel (int channel) {
            currentChannel = channel;
        }

        public String getColor () {
            return color;
        }

    }

