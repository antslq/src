package JNSP_KB.RemoteControl;

public class Main {
    public static void main (String args []) {
        RemoteControl remoteControl = new RemoteControl("Black");

        remoteControl.volumeUp();
        remoteControl.volumeUp();
        remoteControl.volumeUp();

        remoteControl.nextChannel();
        remoteControl.nextChannel();

        System.out.println(remoteControl.getCurrentVolume());
        System.out.println(remoteControl.getCurrentChannel());

        remoteControl.setCurrentVolume(50);
        remoteControl.setCurrentChannel(200);

        System.out.println(remoteControl.getCurrentVolume());
        System.out.println(remoteControl.getCurrentChannel());

        System.out.println(remoteControl.getColor());

    }
}
