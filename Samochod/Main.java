package Samochod;

public class Main {
    public static void main (String args []) {
        //Samochod S1 = new Samochod();
        //Samochod opel = new Samochod();

        Samochod S1 = new Samochod("S1", "X33", 1997, 24556);
        Samochod S2 = new Samochod("Opel", "V33", 1996, 999000);
        System.out.println(S1.marka);
        System.out.println(S1.model);
        System.out.println(S1.rocznik);
        System.out.println(S1.przebieg);

        System.out.println(S2.marka);
        System.out.println(S2.model);
        System.out.println(S2.rocznik);
        System.out.println(S2.przebieg);
/*
        S1.getMarka();
        S1.getModel();
        S1.getRocznik();
        S1.getPrzebieg();
*/
        //S1.pobierzDane();
        //S2.pobierzDane();
        //String test1 = S1.pobierzDane();
        //System.out.println(int );

        S1.setMarka("Volvo");
        S1.setModel("X4");
        S1.setPrzebieg(999000);
        S1.setRocznik(1998);
        System.out.println(S1.getMarka());
        System.out.println(S1.getModel());
        System.out.println(S1.getRocznik());
        System.out.println(S1.getPrzebieg());

        S2.setMarka("Rudy");
        S2.setModel("101");
        S1.setPrzebieg(10000000);
        S1.setRocznik(1941);
        System.out.println(S2.getMarka());
        System.out.println(S2.getModel());
        System.out.println(S2.getRocznik());
        System.out.println(S2.getPrzebieg());

    }
}
