package Samochod;

public class Samochod {
    String marka;
    String model;
    int rocznik;
    int przebieg;

    public Samochod (String marka, String model, int rocznik, int przebieg) {
        this.marka = marka;
        this.model = model;
        this.rocznik = rocznik;
        this.przebieg = przebieg;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setRocznik(int rocznik) {
        this.rocznik = rocznik;
    }

    public void setPrzebieg(int przebieg) {
        this.przebieg = przebieg;
    }


    public String getMarka() {
        return marka;
    }

    public String getModel() {
        return model;
    }

    public int getRocznik() {
        return rocznik;
    }

    public int getPrzebieg() {
        return przebieg;
    }

    public void pobierzDane () {
        getMarka();
        getModel();
        getRocznik();
        getPrzebieg();
    }
}
