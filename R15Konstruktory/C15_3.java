package R15Konstruktory;
//exercise 15.3
//exercise 15.4

public class C15_3 {
    double a;
    char b;

    public C15_3 (double arg_a) {
        this.a = arg_a;        
    }
    
    public C15_3 (char arg_b) {
        this.b = arg_b;
    }
    
    public C15_3 (double arg_a, char arg_b) {
        this (arg_a);
        this.b = arg_b;
    }

    public C15_3 (char arg_b, double arg_a) {
        this (arg_b);
        this.a = arg_a;
    }

}
