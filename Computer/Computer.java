package Computer;


import java.util.ArrayList;
import java.util.List;

//tworzymy pola naszego komputera
public class Computer {
    private Monitor monitor;
    private Drive drive;
    private Headphones headphones;
    //Alt + Enter od razu daje opcje stworzenia kalsy

    //twrzenie listy urządzeń na USB
    //po utworzeniu klasy trzeba zaiportować listę. Najeżdząmy na list i Alt + Enter
    List<USBDevices> usbDevices = new ArrayList<>();

    //przypisnie argumentów do pól klasy Alt + Enter i Bind constructor parameters to field
    public Computer (Monitor monitor, Drive drive) {

        this.monitor = monitor;
        this.drive = drive;
    }

    //tworzymy getery do podania informacji jakie słuchawki/monitor czy HDD mamy podpiete

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Drive getDrive() {
        return drive;
    }

    public void setDrive(Drive drive) {
        this.drive = drive;
    }

    public Headphones getHeadphones() {
        return headphones;
    }

    public void setHeadphones(Headphones headphones) {
        this.headphones = headphones;
    }

    /*public List <USBDevices> getUsbDevices () {
        this.usbDevices = usbDevices;
    }*/
}
