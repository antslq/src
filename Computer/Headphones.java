package Computer;

public class Headphones {
    private final String name;

    public Headphones(String name) {
        this.name = name;
    }

    //tworzymy getera do podania modelu słuchawek

    public String getName() {
        return name;
    }
}
