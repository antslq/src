package Uzytkownik;

public class Uzytkownik {
    private int wiek;
    private String imie;

    //tworzymy konstruktor - prawy klawisz myszy, Generate > Constructor
    public Uzytkownik (int wiek, String imie) {
        this.wiek = wiek;
        this.imie = imie;
    }

    //tworzymy getery i setery żeby nie upubliczniać danych z klasy (private)
    public int getWiek() {
        return wiek;
    }

    public String getImie() {
        return imie;
    }

    //tworymy setera z typem void bo nic nie zwraca tylko uztsawia wartość. Podajemy w nawiasie argument który ustawiamy (int wiek)
    //potem tą wartść tzreba przypisać do pola this.wiek (zonacza to pole klasy). Wiek za = to ten kktóry jest argumentem w nawiasie.

    public void setWiek(int wiek) {
        if (wiek > 0) {
            this.wiek = wiek;
        } else {
            //to na dole to wyjątek - jedna z technik obsługi błędów
            throw new RuntimeException(" Wiek musi być większy od 0");
        }
    }

    public void setImie(String imie) {
        this.imie = imie;
    }
}
