package Prostokat;

public class Prostokat {
    Punkt LewyGorny;
    Punkt LewyDolny;
    Punkt PrawyGorny;
    Punkt PrawyDolny;

    Punkt lewyGorny = new Punkt();
    Punkt lewyDolny = new Punkt();
    Punkt prawyGorny = new Punkt();
    Punkt prawyDolny = new Punkt();

    int pobierzLGX() {
        return LewyGorny.x;
    }

    int pobierzLGY() {
        return LewyGorny.y;
    }

    int pobierzLDX() {
        return LewyDolny.x;
    }

    int pobierzLDY() {
        return LewyDolny.y;
    }

    int pobierzPGX() {
        return PrawyDolny.x;
    }

    int pobierzPGY() {
        return PrawyGorny.y;
    }

    int pobierzPDX() {
        return PrawyDolny.x;
    }

    int pobierzPDY() {
        return PrawyDolny.y;
    }

    public void wyswietlWspolrzedne() {
        System.out.println("LG=" + pobierzLGX() + pobierzLGY());
        System.out.println("LD=" + pobierzLDX() + pobierzLDY());
        System.out.println("PG=" + pobierzPGX() + pobierzPGY());
        System.out.println("PD=" + pobierzPDX() + pobierzPDY());
    }

    boolean przyrownanie() {
            if  ((pobierzLGX() != pobierzLDX()) ||
                    (pobierzPGX() != pobierzPDX()) ||
                (pobierzLGY() != pobierzPGY()) ||
                (pobierzPDY() != pobierzLDY())) {
            return false;
        } else return true;
    }
}
