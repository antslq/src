package Prostokat;

public class Punkt {

    //to są pola do przechowywania danych
    public int x;
    public int y;

    int pobierzX () {
        return x;
    }
    int pobierzY () {
        return y;
    }

    void wyswietlWspolrzedne () {
        System.out.println("wspolrzedna x =" + x);
        System.out.println("wspolrzedna y =" + y);
    }
}
    //deklaracja zmiennej typu Punkt - typ_zmiennej nazwa_zmiennej;
    //Punkt przykladowyPunkt; - zmienna odnośnikowa/referencyjna/obiektowa

    //utworzenie obiektu klasy Punkt i powiązanie go ze zmienną new nazwa_klasy ();
    //nazwa_klasy nazwa_zmiennej = new nazwa_klasy ();
    //Punkt przykladowyPunkt = new Punkt();

