package Garnek;

public class Garnek {
    int srednica;//final oznacza że nie można zmienić wartości
    int wysokosc;
    String kolor;

    public Garnek() {

    }

    public Garnek (int srednica, int wysokosc, String kolor) {
        this.srednica = srednica; //przypisujemy argumenty do właściwych pól klasy
        //this. oznacza że przekazujemy wartość argumentu srednica do pola srednica pochodzącego z konstruktora
        this.wysokosc = wysokosc;
        this.kolor = kolor;
    }

    public String gotuj() {
        return "Gotowanie w trakcie.";
    }
    //teraz przeciążamy - tworzymy taką samą metodę, też zwracającą stringa ale przyjmującą inne argumenty
    public String gotuj(boolean CzyDodalismySol) {
        if (CzyDodalismySol) {
            return "Gotowanie z solą";
        } else {
            return "Gotowanie bez soli";
        }
    }

    public int ZwrocTemperatureWrzeniaWody () {
        return 100;
    }

    public void GotujBezInformacji () {
        WlaczGarnek();
        GotujPrzezPolGodziny();
        WylaczGarnek();
    }

    public void WlaczGarnek () {
        System.out.println("Włączanie garnka");
    }

    public void GotujPrzezPolGodziny () {
        System.out.println("Gotowanie przez 30 minut");
    }

    public void WylaczGarnek () {
        System.out.println("Wyłączanie garnka");
    }
}
