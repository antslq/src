package Garnek;

public class Main {
    public static void main (String args []) {
        /*
    Garnek garnek = new Garnek();
//teraz nastąpi przypisanie wartości do pól obiektu
        garnek.srednica = 7;
        garnek.wysokosc = 14;
        garnek.kolor = "srebrny";

        System.out.println(garnek.srednica);
        System.out.println(garnek.wysokosc);
        System.out.println(garnek.kolor);

         */
        Garnek garnek = new Garnek(7, 15, "srebrny");

        String wiadomosc = garnek.gotuj(); //przypisujemy to co zwraca ta metoda do zmiennej
        System.out.println(wiadomosc);  //String zwrócony z metody gotuj w Klasie Garnek

        String wiadomosc2 = garnek.gotuj(true);
        System.out.println(wiadomosc2);

        int temperatura = garnek.ZwrocTemperatureWrzeniaWody();
        System.out.println(temperatura);

        garnek.GotujBezInformacji();
    }
}
