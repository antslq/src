package JNSP_KB_UZYTKOWNIK;

public class RejestratorUzytkownikow {
    public String rejestruj (String nazwaUzytkownika, String haslo) {
        if (czyUzytkownikIstnieje (nazwaUzytkownika)) {
            return "Użytkownik niezarejestrowany - taki użytkownik nie istnieje!";
        }
        if (czyNazwaUzytkownikaZawieraNiedozwoloneZnaki (nazwaUzytkownika)) {
            return "Użytkownik niezarestrowany - nazwa użytkownika zawiera niedozwolone znaki!";
        }
        if (czyHasloJestZbytProste (haslo)) {
            return "Użytkownik niezarejestrowany - hasło jest zbyt prost!";
        }
        return "Użytkownik zarejestrowany!";
    }

    private boolean czyUzytkownikIstnieje (String nazwaUzytkownika) {
        return false;
    }
    private boolean czyNazwaUzytkownikaZawieraNiedozwoloneZnaki (String nazwaUzytkownika) {
        return nazwaUzytkownika.contains("#") || nazwaUzytkownika.contains("&");
    }
    private boolean czyHasloJestZbytProste (String haslo) {
        return haslo.length() <=8;
    }
}
